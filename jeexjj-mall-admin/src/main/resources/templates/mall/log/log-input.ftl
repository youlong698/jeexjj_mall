<#--
/****************************************************
 * Description: t_mall_log的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/log/save" id=tabId>
   <input type="hidden" name="id" value="${log.id}"/>
   
   <@formgroup title='name'>
	<input type="text" name="name" value="${log.name}" >
   </@formgroup>
   <@formgroup title='type'>
	<input type="text" name="type" value="${log.type}" check-type="number">
   </@formgroup>
   <@formgroup title='url'>
	<input type="text" name="url" value="${log.url}" >
   </@formgroup>
   <@formgroup title='request_type'>
	<input type="text" name="requestType" value="${log.requestType}" >
   </@formgroup>
   <@formgroup title='request_param'>
	<input type="text" name="requestParam" value="${log.requestParam}" >
   </@formgroup>
   <@formgroup title='user'>
	<input type="text" name="user" value="${log.user}" >
   </@formgroup>
   <@formgroup title='ip'>
	<input type="text" name="ip" value="${log.ip}" >
   </@formgroup>
   <@formgroup title='ip_info'>
	<input type="text" name="ipInfo" value="${log.ipInfo}" >
   </@formgroup>
   <@formgroup title='time'>
	<input type="text" name="time" value="${log.time}" check-type="number">
   </@formgroup>
   <@formgroup title='create_date'>
	<@date name="createDate" dateValue=log.createDate  default=true/>
   </@formgroup>
</@input>